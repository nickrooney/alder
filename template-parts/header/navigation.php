<nav class="site-nav">
    <div class="nav-wrapper">
        <a href="#" class="close nav-trigger">×</a>

        <?php get_template_part('template-parts/header/nav-links'); ?>
    </div>
</nav>