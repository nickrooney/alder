<?php 
	$link = get_field('footer_membership_banner_link', 'options');
	if( $link ): 
	$link_url = $link['url'];
	$link_title = $link['title'];
	$link_target = $link['target'] ? $link['target'] : '_self';
 ?>

	<?php if(!is_page_template('templates/membership.php')):?>

		<div class="membership-banner">
			<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		</div>

	 <?php endif; ?>

<?php endif; ?>
