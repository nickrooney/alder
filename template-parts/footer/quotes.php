<?php

$repeater = get_field('footer_quotes', 'options');
$rand = rand(0, (count($repeater) - 1));
$quote = $repeater[$rand]['quote'];
$source = $repeater[$rand]['source'];

?>




<?php if(have_rows('footer_quotes', 'options')): ?>
    
    <div class="quotes">        
        <blockquote>
            <?php echo $quote; ?>
        </blockquote>

        <div class="source">
            <h5 class="sub-header gray-blue"><?php echo $source; ?></h5>
        </div>                

        <div class="ornament">
            <img src="<?php bloginfo('template_directory'); ?>/images/bg-tree-footer.png" alt="Alder Tree" />
        </div>
    </div>

<?php endif; ?>