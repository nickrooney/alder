<?php if(have_rows('social', 'options')): ?>
    
    <div class="social-links">
        <?php while(have_rows('social',  'options')): the_row(); ?>
    
            <div class="social-link">
                <a href="<?php the_sub_field('link'); ?>" target="_window">
                    <img src="<?php $icon = get_sub_field('icon'); echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
                </a>
            </div>

        <?php endwhile; ?>
    </div>
    
<?php endif; ?>