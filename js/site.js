(function ($, window, document, undefined) {

	$(document).ready(function($) {


		// Nav Trigger
		$('.nav-trigger').click(function(){
			$('body').toggleClass('nav-overlay-open');
			return false;
		});

		// FAQs
		$('.faq .question').on('click', function(){
			$('.faq').removeClass('active');
			let faq = $(this).closest('.faq');
			faq.toggleClass('active');


			return false;
		});

		$('.referrals .referrals-toggle a').on('click', function(){
			let form = $('.referral-form');
			$(form).toggleClass('active');

			return false;
		});

		$('.membership-info .membership-toggle a').on('click', function(){
			let form = $('.membership-form');
			$(form).toggleClass('active');

			return false;
		});



	});

	// Close on click outside nav
	$(document).mouseup(function(e) {
		var site_nav = $('.site-nav');
	
		if (!site_nav.is(e.target) && site_nav.has(e.target).length === 0) {
			$('body').removeClass('nav-overlay-open');
		}	
	});

	// Close on ESC
	$(document).keyup(function(e) {		
		if (e.keyCode == 27) {
			$('body').removeClass('nav-overlay-open');
		}
	});

	$(window).scroll(function() {
		var elementTarget = $('.home-hero'),
			heroHeight = elementTarget.height();

		if ($(this).scrollTop() > heroHeight) {
			$('.site-header').addClass('sticky');
		} else {
			$('.site-header').removeClass('sticky');
		}
	});


})(jQuery, window, document);