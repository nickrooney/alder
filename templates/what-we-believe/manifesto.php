<?php

$manifesto = get_field('manifesto');
$headline = $manifesto['headline'];
$copy = $manifesto['copy'];


?>

<section class="manifesto grid">
    <div class="ornament ornament-top">  
        <img src="<?php bloginfo('template_directory'); ?>/images/bg-topo-blue.svg" alt="Topo Map" />
    </div>

    <div class="headline">
        <h3 class="section-title"><?php echo $headline; ?></h3>
    </div>

    <div class="copy copy-2 extended">
        <?php echo $copy; ?>
    </div>

    <div class="ornament ornament-bottom">
        <img src="<?php bloginfo('template_directory'); ?>/images/bg-circle-dots-gold.svg" alt="Circle Dots" />
    </div>

</section>