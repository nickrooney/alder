<?php

$about = get_field('about');
$copy = $about['copy'];

?>

<section class="about grid">

    <div class="copy copy-2 extended">
        <?php echo $copy; ?>
    </div>

</section>