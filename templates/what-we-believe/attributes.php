<?php

    $headline = get_field('attributes_headline');
    $copy = get_field('attributes_copy');

?>

<?php if(have_rows('attributes')): ?>
    <section class="attributes grid">

        <div class="section-header">
            <h3 class="section-title"><?php echo $headline; ?></h3>
        </div>

        <div class="header-copy copy copy-2">
            <?php echo $copy; ?>
        </div>

        <div class="attributes-grid">
            <?php $count = 1; while(have_rows('attributes')): the_row(); ?>

                <div class="attribute attribute-<?php echo $count; ?>">
                    <div class="sub-headline">
                        <h3 class="sub-header"><?php the_sub_field('headline'); ?></h3>
                    </div>

                    <div class="copy copy-3">
                        <?php the_sub_field('deck'); ?>
                    </div>                        
                </div>

            <?php $count++; endwhile; ?>
        </div>

    </section>

<?php endif; ?>