<?php

    $headline = get_field('values_headline');
    $credits = get_field('values_illustration_credits');
?>

<?php if(have_rows('values')): ?>
    <section class="values grid">

        <div class="section-header">
            <h3 class="section-title"><?php echo $headline; ?></h3>
        </div>

        <div class="values-grid">
            <?php $count = 1; while(have_rows('values')): the_row(); ?>

                <div class="value value-<?php echo $count; ?>">
                    <div class="photo">
                        <?php $image = get_sub_field('image'); if( $image ): ?>
                            <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
                        <?php endif; ?>
                    </div>

                    <div class="info">
                        <div class="headline">
                            <h4><?php the_sub_field('headline'); ?></h4>
                        </div>

                        <div class="copy copy-3">
                            <?php the_sub_field('deck'); ?>
                        </div> 
                    </div>                       
                </div>

            <?php $count++; endwhile; ?>
        </div>

        <div class="credits copy copy-3">
            <p><?php echo $credits; ?></p>
        </div>

    </section>

<?php endif; ?>