<?php

$referrals = get_field('referrals');
$headline = $referrals['headline'];
$copy = $referrals['copy'];
$form = $referrals['form_shortcode'];

?>

<section class="referrals grid">

    <div class="info">
        <div class="headline">
            <h3><?php echo $headline; ?></h3>
        </div>

        <div class="copy copy-3 extended">
            <?php echo $copy; ?>
        </div>

        <div class="referrals-toggle cta">
            <a href="#" class="btn white">Refer a new member</a>
        </div>

        <div class="referral-form">
            <?php echo do_shortcode($form); ?>
        </div>
    </div>

</section>