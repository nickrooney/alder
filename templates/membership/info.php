<?php

$info = get_field('about');
$headline = $info['headline'];
$copy = $info['copy'];
$link = $info['link'];
$form = $info['form_shortcode'];

?>

<section class="info membership-info grid">

    <div class="headline">
        <h3 class="section-title gold"><?php echo $headline; ?></h3>
    </div>

    <div class="copy copy-3 extended">
        <?php echo $copy; ?>
    </div>

    <div class="membership-toggle cta">
        <a href="#" class="btn">Begin the application process</a>
    </div>

    <div class="membership-form">
        <?php echo do_shortcode($form); ?>
    </div>

</section>