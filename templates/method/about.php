<?php

$about = get_field('about');
$copy = $about['copy'];
$bottom_copy = $about['bottom_copy'];
$link = $about['link'];

if(have_rows('about')): while(have_rows('about')): the_row();

?>

    <section class="about grid">

        <div class="copy copy-2 extended">
            <?php echo $copy; ?>
        </div>

        <?php if(have_rows('levels')): ?>
            <div class="levels">
                
                <?php $count = 1; while(have_rows('levels')): the_row(); ?>
    
                    <div class="level feature-<?php echo $count; ?>">
                        <div class="photo">
                            <?php $image = get_sub_field('graphic'); if( $image ): ?>
                                <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
                            <?php endif; ?>
                        </div>

                        <div class="copy copy-3">
                            <?php the_sub_field('deck'); ?>
                        </div>                        
                    </div>

                <?php $count++; endwhile; ?>

            </div>

        <?php endif; ?>

        <div class="copy bottom copy-2 extended">
            <?php echo $bottom_copy; ?>
        </div>

        <?php 
            if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

            <div class="cta">
                <a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
            </div>

        <?php endif; ?>

    </section>

<?php endwhile; endif; ?>
