<?php

$history = get_field('history');
$headline = $history['headline'];
$copy = $history['copy'];
$photo = $history['photo'];

?>

<section class="history grid">
    
    <div class="headline">
        <h3 class="section-title"><?php echo $headline; ?></h3>
    </div>

    <?php if($photo): ?>
        <div class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>
    <?php endif; ?>

    <div class="copy copy-3 extended">
        <?php echo $copy; ?>
    </div>

</section>