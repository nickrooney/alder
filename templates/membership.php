<?php
/*

    Template Name: Membership

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/hero'); ?>

    <?php get_template_part('templates/membership/info'); ?>

    <?php get_template_part('templates/membership/referrals'); ?>

<?php get_footer(); ?>