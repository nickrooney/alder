<?php
/*

    Template Name: What We Believe

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/hero'); ?>

    <?php get_template_part('templates/what-we-believe/about'); ?>
    
    <?php get_template_part('templates/what-we-believe/manifesto'); ?>

    <?php get_template_part('templates/what-we-believe/values'); ?>

    <?php get_template_part('templates/what-we-believe/attributes'); ?>

<?php get_footer(); ?>