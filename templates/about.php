<?php
/*

    Template Name: About

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/hero'); ?>

    <?php get_template_part('templates/about/staff'); ?>

    <?php get_template_part('templates/about/ceo'); ?>

    <?php get_template_part('templates/about/history'); ?>

<?php get_footer(); ?>